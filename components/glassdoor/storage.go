package glassdoor

import (
	"gitlab.com/qrsx/core/models"
	"io/ioutil"
	"os"
)

type Storage struct {
	path string
}

func NewStorage(path string) *Storage {
	return &Storage{path: path}
}

func (s *Storage) Fetch() (map[string]models.GlassdoorCompanyComplete, error) {
	content, err := ioutil.ReadFile(s.path)

	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}

		return nil, err

	}

	var companies = make(models.GlassdoorCompanyCompleteList, 0, 1024)

	err = companies.UnmarshalJSON(content)
	if err != nil {
		return nil, err
	}

	var result = make(map[string]models.GlassdoorCompanyComplete, len(companies))

	for _, company := range companies {
		result[company.DouCompanyAlias] = company
	}

	return result, nil
}
