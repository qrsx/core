package dumper

import (
	. "gitlab.com/qrsx/core/models"
	. "gitlab.com/qrsx/dou-scrapper/models"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
)

func Dump(companies []dou.FullCompany) ([]byte, error) {
	zip := ZipCompanies(zipCompanies(companies))

	return zip.MarshalJSON()
}

func zipCompanies(companies []dou.FullCompany) []ZipCompany {
	result := make([]ZipCompany, len(companies))

	for i, company := range companies {
		result[i] = ZipCompany{
			/* Alias:         */ company.Alias,
			/* Name:          */ company.Name,
			/* Offices:       */ zipOffices(company.Offices),
			/* Vacancies:     */ zipVacancies(company.Vacancies),
			/* ReviewCount:   */ company.ReviewCount,
			/* EmployeeCount: */ company.EmployeeCount,
			/* CompanyType:   */ company.CompanyType,
			/* InTop50Largest:*/ company.InTop50Largest,
			/* PhotoExists:	  */ company.PhotoExists,
		}
	}

	return result
}

func zipVacancies(source []dou.FullVacancy) []ZipVacancy {
	result := make([]ZipVacancy, len(source))

	for i, vacancy := range source {
		result[i] = ZipVacancy{
			/* Id:        	 	*/ vacancy.Id,
			/* Title:        	*/ vacancy.Title,
			/* Cities:       	*/ zipCityList(vacancy.Cities),
			/* ExistsOffice: 	*/ vacancy.ExistsOffice,
			/* Salary:       	*/ vacancy.Salary,
			/* Published:       */ vacancy.Published,
			/* Remote:       	*/ vacancy.Remote,
		}
	}

	return result
}

func zipOffices(source []dou.FullOffice) []ZipCompanyOffice {
	result := make([]ZipCompanyOffice, len(source))

	for i, office := range source {
		result[i] = ZipCompanyOffice{
			/* City:     */ zipOneCity(office.City),
			/* Address:  */ office.Address,
			/* Location: */ ZipLocation{
				round(office.Location.Latitude),
				round(office.Location.Longitude),
			},
		}
	}

	return result
}

func zipCityList(source []City) []ZipCity {
	result := make([]ZipCity, len(source))

	for i, city := range source {
		result[i] = zipOneCity(city)
	}

	return result
}

func zipOneCity(source City) ZipCity {
	return ZipCity{
		source.Alias,
		source.Name,
	}
}

func round(f float64) float64 {
	const precision = 1e7

	return float64(int64(f*precision)) / precision
}
