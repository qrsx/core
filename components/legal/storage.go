package legal

import (
	"gitlab.com/qrsx/core/models"
	"io/ioutil"
	"os"
)

type Storage struct {
	path string
}

func NewStorage(path string) *Storage {
	return &Storage{path: path}
}

func (s *Storage) Fetch() (map[string]models.LegalCompany, error) {
	content, err := ioutil.ReadFile(s.path)

	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}

		return nil, err

	}

	var soicals = make(models.LegalCompanyList, 0, 1024)

	err = soicals.UnmarshalJSON(content)
	if err != nil {
		return nil, err
	}

	var result = make(map[string]models.LegalCompany, len(soicals))

	for _, social := range soicals {
		result[social.DouCompanyAlias] = social
	}

	return result, nil
}
