package git

import (
	"github.com/juju/errors"
)

func CloneAndPull(projectGit, projectLocalPath, authorName, authorEmail, branch string) error {
	err := Clone(projectGit, projectLocalPath, branch)
	if err != nil {
		return errors.Trace(err)
	}

	err = Pull(projectLocalPath)
	if err != nil {
		return errors.Trace(err)
	}

	err = ConfigUserName(projectLocalPath, authorName)
	if err != nil {
		return errors.Trace(err)
	}

	err = ConfigUserEmail(projectLocalPath, authorEmail)
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}

func CommitAndPush(projectLocalPath, message string) error {
	err := Commit(projectLocalPath, message)
	if err != nil {
		return errors.Trace(errors.Errorf("path %s with %+v", projectLocalPath, err))
	}

	err = Push(projectLocalPath)
	if err != nil {
		return errors.Trace(errors.Errorf("path %s with %+v", projectLocalPath, err))
	}

	return nil
}
