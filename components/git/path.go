package git

import (
	"hash/fnv"
	"strconv"
	"strings"
)

// unsafe
func LocalPath(fullUrl string) string {
	var (
		packageIndex             = strings.Index(fullUrl, "@")
		packageName              = fullUrl[packageIndex+1:]
		repositoryNameStartIndex = strings.LastIndex(packageName, "/")
		repositoryNameEndIndex   = strings.LastIndex(packageName, ".")
	)

	return packageName[repositoryNameStartIndex+1:repositoryNameEndIndex] + "-" + fnvSum(packageName)
}

func fnvSum(repository string) string {
	hash := fnv.New64()

	_, _ = hash.Write([]byte(repository))

	return strconv.FormatUint(hash.Sum64(), 32)
}
