package git

import (
	"fmt"
	"os"
	"os/exec"
)

func Clone(url, path, branch string) error {
	_, err := os.Stat(path)

	if os.IsNotExist(err) {
		// All fine
		cmd := exec.Command("git", "clone", "--branch", branch, url, path)

		return cmd.Run()
	}

	return err
}

func Pull(path string) error {
	cmd := exec.Command("git", "pull")
	cmd.Dir = path

	return cmd.Run()
}

func ConfigUserName(path, value string) error {
	cmd := exec.Command("git", "config", "user.name", fmt.Sprintf("%q", value))
	cmd.Dir = path

	return cmd.Run()
}

func ConfigUserEmail(path, value string) error {
	cmd := exec.Command("git", "config", "user.email", fmt.Sprintf("%q", value))
	cmd.Dir = path

	return cmd.Run()
}

func Add(path, file string) error {
	cmd := exec.Command("git", "add", file)
	cmd.Dir = path

	return cmd.Run()
}

func Commit(path, message string) error {
	cmd := exec.Command("git", "commit", "--allow-empty", "-m", message)
	cmd.Dir = path

	return cmd.Run()
}

func Push(path string) error {
	cmd := exec.Command("git", "push")
	cmd.Dir = path

	return cmd.Run()
}
