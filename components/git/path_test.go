package git

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLocalPath(t *testing.T) {
	assert.Equal(t, "assets-lrroin7s58ku", LocalPath("https://qrsx-bot:xxxxx@github.com/qrsx/assets.git"))
	assert.Equal(t, "assets-lrroin7s58ku", LocalPath("https://qrsx-bot:yyyyy@github.com/qrsx/assets.git"))
}
