package models

// easyjson:json
type ZipLocation [2]float64

// easyjson:json
type ZipLocations []ZipLocation

// easyjson:json
type ZipCity [2]string // name, original

// easyjson:json
type ZipCompanyOffice [3]interface {
	// City     ZipCity
	// Address  string
	// Location Location
}

// easyjson:json
type ZipVacancy [7]interface {
	// Id           	int
	// Title        	string
	// Cities       	[]string
	// OfficeExists 	bool
	// Salary       	string
	// Published       	string
	// Remote       	bool
}

// easyjson:json
type ZipCompany [9]interface {
	// Alias         string
	// Name          string
	// Offices       []ZipCompanyOffice
	// Vacancies     []ZipVacancy
	// ReviewCount   int
	// EmployeeCount string
	// PhotoExists 	 bool
}

// easyjson:json
type ZipCompanies []ZipCompany
