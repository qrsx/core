package models

type GitProject struct {
	AuthorEmail string
	AuthorName  string
	Url         string
	Branch      string
}
