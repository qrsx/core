package models

// easyjson:json
type FullSocial struct {
	DouCompanyAlias string `json:"dou_company_alias"`
	GithubURL       string `json:"github_url"`
	GitlabURL       string `json:"gitlab_url"`
	LinkedinURL     string `json:"linkedin_url"`
	DevtoURL        string `json:"devto_url"`
	FacebookURL     string `json:"facebook_url"`
	InstagramURL    string `json:"instagram_url"`
}

// easyjson:json
type FullSocialList []FullSocial
