package models

type GlassdoorCompanyComplete struct {
	DouCompanyAlias             string  `json:"dou_company_alias"`
	GlassdoorWorkingCompanyCode string  `json:"glassdoor_working_company_code"`
	GlassdoorCompanyAlias       string  `json:"glassdoor_company_alias"`
	GlassdoorCompanyCode        string  `json:"glassdoor_company_code"`
	VacancyCount                uint32  `json:"vacancy_count"`
	ReviewCount                 uint32  `json:"review_count"`
	PhotoCount                  uint32  `json:"photo_count"`
	Rating                      float64 `json:"rating"`
}

// easyjson:json
type GlassdoorCompanyCompleteList []GlassdoorCompanyComplete
