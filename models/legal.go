package models

type LegalCompany struct {
	DouCompanyAlias string `json:"dou_company_alias"`
	LegalID         uint64 `json:"legal_id"`
}

// easyjson:json
type LegalCompanyList []LegalCompany
