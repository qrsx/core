# Service to update [devspace.com.ua](https://devspace.com.ua/) by cron

### Before run need create *credentials.go*
```go
package main

import (
	"gitlab.com/qrsx/core/models"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
)

func init() {
	scrapperCredentials = dou.Credentials{
		UserAgent:               "xxxxxxxxxxxxxxx",
		CookieCSFRToken:         "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
		BodyCSFRMiddlewareToken: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
	}
	gmapApiKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
	assetsGitProject = models.GitProject{
		AuthorEmail: "qrsx.bot@example.com",
		AuthorName:  "qrsx-bot",
		Url:         "https://qrsx-bot:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx@github.com/qrsx/assets.git",
		Branch:      "master",
	}
	frontendGitProjects = []models.GitProject{
		{
			AuthorEmail: "qrsx.bot@example.com",
			AuthorName:  "qrsx-bot",
			Url:         "https://qrsx-bot:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx@gitlab.com/qrsx/devspace.com.ua.git",
			Branch:      "hot-reload",
		},
		// deploy to multi repositories
	}
}
```

### Run
```bash
make install
make build
qrsx
```