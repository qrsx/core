package views

import "gitlab.com/qrsx/core/models"

type CompanyPage struct {
	Alias         string
	Name          string
	Logo          string
	SiteURL       string
	Description   string
	EmployeeCount string
	ReviewCount   int
	VacancyCount  int
	Glassdoor     models.GlassdoorCompanyComplete
	Social        models.FullSocial
	Legal         uint64
}
