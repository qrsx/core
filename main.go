package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/juju/errors"
	"github.com/robfig/cron"
	"github.com/valyala/fasthttp"
	typeclient "gitlab.com/qrsx/companytype-client"
	"gitlab.com/qrsx/core/components/dumper/dumper"
	"gitlab.com/qrsx/core/components/git"
	"gitlab.com/qrsx/core/components/glassdoor"
	"gitlab.com/qrsx/core/components/legal"
	"gitlab.com/qrsx/core/components/social"
	"gitlab.com/qrsx/core/models"
	"gitlab.com/qrsx/core/views"
	"gitlab.com/qrsx/dou-scrapper/components/dou"
	"gitlab.com/qrsx/dou-scrapper/components/gmap"
	"gitlab.com/qrsx/dou-scrapper/components/logger"
	. "gitlab.com/qrsx/dou-scrapper/models/dou"
	"io/ioutil"
	"os"
	"strings"
	"sync"
	"time"
)

const companyRequestLimit = 100

const (
	vacancyStoragePath   = "/var/proto.storage/dou/vacancy.storage"
	companyStoragePath   = "/var/proto.storage/dou/company.storage"
	reviewStoragePath    = "/var/proto.storage/dou/review.storage"
	locationStoragePath  = "/var/proto.storage/gmap/location.storage"
	glassdoorStoragePath = "/var/proto.storage/external/glassdoor.json"
	socialStoragePath    = "/var/proto.storage/external/socials.json"
	legalsStoragePath    = "/var/proto.storage/external/legals.json"

	typeClientPath = "https://devspace-com-ua.herokuapp.com/state/result.json"

	gitProjectPath    = "/var/projects.storage/"
	resultStoragePath = "/var/proto.storage/dou/companies.json"

	verifyCompanyPath = "/var/proto.storage/dou/verify.json"

	assetRelativePathFormat = "/y2006/m01/d02"
	lastmodFormat           = "2006-01-02"
	assetVersionFileFormat  = "15-04.json"
)

var (
	scrapperCredentials Credentials
	gmapApiKey          string
	assetsGitProject    models.GitProject
	frontendGitProjects []models.GitProject
)

var (
	kyiv *time.Location
)

func init() {
	var err error

	kyiv, err = time.LoadLocation("Europe/Kiev")

	if err != nil {
		logger.Critical(errors.Trace(err))
	}
}

func main() {
	runScrapping()
}

func debuggingAssetsGit() {
	emptyGitProject := models.GitProject{}
	if assetsGitProject == emptyGitProject {
		logger.Criticalf("[GIT] assets project is empty, please fill")
	}

	assetsLocalPath := gitProjectPath + git.LocalPath(assetsGitProject.Url)

	err := git.CloneAndPull(
		assetsGitProject.Url,
		assetsLocalPath,
		assetsGitProject.AuthorName,
		assetsGitProject.AuthorEmail,
		assetsGitProject.Branch,
	)
	if err != nil {
		logger.Critical(errors.Trace(err))
	}

	err = os.MkdirAll(assetsLocalPath+assetRelativePath(), 0666)
	if err != nil {
		logger.Critical(errors.Trace(err))
	}
}

func debuggingFrontendGit() {
	refreshFrontendProjects(time.Now().String(), nil, nil)
}

func assertFrontendProjects() {
	if len(frontendGitProjects) == 0 {
		logger.Criticalf("[GIT] frontend projects is empty, please fill")
	}

	for _, project := range frontendGitProjects {
		localPath := gitProjectPath + git.LocalPath(project.Url)

		startTime := time.Now()
		err := git.CloneAndPull(project.Url, localPath, project.AuthorName, project.AuthorEmail, project.Branch)
		runDuration := time.Since(startTime)
		logger.Infof("[GIT][FRONTEND PULL] duration %.3f", asSecond(runDuration))

		if err != nil {
			logger.Critical(errors.Trace(err))
		}
	}
}

func assertAssetsProject() error {
	localPath := gitProjectPath + git.LocalPath(assetsGitProject.Url)

	startTime := time.Now()
	err := git.CloneAndPull(assetsGitProject.Url, localPath, assetsGitProject.AuthorName, assetsGitProject.AuthorEmail, assetsGitProject.Branch)
	runDuration := time.Since(startTime)
	logger.Infof("[GIT][ASSETS PULL] duration %.3f", asSecond(runDuration))

	return err
}

func refreshFrontendProjects(dataProvider string, companies []FullCompany, companyAliases []string) {
	var glassdoorStorage = glassdoor.NewStorage(glassdoorStoragePath)
	var glassdoorAliasCompanyMap, glassdoorFetchErr = glassdoorStorage.Fetch()
	if glassdoorFetchErr != nil {
		logger.Error(errors.Trace(glassdoorFetchErr))
	}

	var socialStorage = social.NewStorage(socialStoragePath)
	var socialAliasCompanyMap, soicalFetchErr = socialStorage.Fetch()
	if soicalFetchErr != nil {
		logger.Error(errors.Trace(soicalFetchErr))
	}

	var legalStorage = legal.NewStorage(legalsStoragePath)
	var legalAliasCompanyMap, legalFetchErr = legalStorage.Fetch()
	if legalFetchErr != nil {
		logger.Error(errors.Trace(legalFetchErr))
	}

	for i, frontendGitProject := range frontendGitProjects {
		localPath := gitProjectPath + git.LocalPath(frontendGitProject.Url)

		// for devspace.com.ua
		firstOnly := i == 0

		err := git.CloneAndPull(
			frontendGitProject.Url,
			localPath,
			frontendGitProject.AuthorName,
			frontendGitProject.AuthorEmail,
			frontendGitProject.Branch,
		)
		if err != nil {
			logger.Error(errors.Trace(err))

			continue
		}

		staticPagePath := localPath + "/public"
		files, err := ioutil.ReadDir(staticPagePath)
		if err != nil {
			logger.Error(errors.Trace(err))

			continue
		}

		for _, file := range files {
			templateFilename := file.Name()

			if strings.HasSuffix(templateFilename, ".html.template") {
				resultFilename := staticPagePath + "/" + strings.TrimSuffix(templateFilename, ".template")

				template, err := ioutil.ReadFile(staticPagePath + "/" + templateFilename)
				if err != nil {
					logger.Critical(errors.Trace(err))
				}

				resultContent := bytes.Replace(template, []byte("{DATA_PROVIDER}"), []byte(dataProvider), -1)

				err = ioutil.WriteFile(resultFilename, resultContent, 0666)
				if err != nil {
					logger.Critical(errors.Trace(err))
				}

				err = git.Add(localPath, resultFilename)
				if err != nil {
					logger.Critical(errors.Trace(err))
				}
			}
		}

		if firstOnly {
			{
				var content = views.SitemapCompaniesTemplate(companyAliases, lastmod())

				var sitemapFilename = staticPagePath + "/sitemap-companies.xml"

				err := ioutil.WriteFile(sitemapFilename, []byte(content), 0666)
				if err != nil {
					logger.Critical(errors.Trace(err))
				}

				err = git.Add(localPath, sitemapFilename)
				if err != nil {
					logger.Critical(errors.Trace(err))
				}
			}

			companiesStaticPagePath := staticPagePath + "/companies"
			err = os.MkdirAll(companiesStaticPagePath, 0666)
			if err != nil {
				logger.Critical(errors.Trace(err))
			}

			for _, company := range companies {
				var content = views.CompanyPageTemplate(views.CompanyPage{
					Alias:         company.Alias,
					Name:          company.Name,
					Logo:          company.Logo,
					Description:   "",
					SiteURL:       company.SiteURL,
					EmployeeCount: company.EmployeeCount,
					ReviewCount:   company.ReviewCount,
					VacancyCount:  len(company.Vacancies),
					Glassdoor:     glassdoorAliasCompanyMap[company.Alias],
					Social:        socialAliasCompanyMap[company.Alias],
					Legal:         legalAliasCompanyMap[company.Alias].LegalID,
				})

				resultFilename := companiesStaticPagePath + "/" + company.Alias + ".html"

				err = ioutil.WriteFile(resultFilename, []byte(content), 0666)
				if err != nil {
					logger.Critical(errors.Trace(err))
				}

				err = git.Add(localPath, resultFilename)
				if err != nil {
					logger.Critical(errors.Trace(err))
				}
			}
		}

		err = git.CommitAndPush(localPath, "update CDN source by @bot")
		if err != nil {
			logger.Critical(errors.Trace(err))
		}
	}
}

func runScrapping() {
	emptyScrapperCredentials := Credentials{}
	if scrapperCredentials == emptyScrapperCredentials {
		logger.Criticalf("[DOU] credentials is empty, please fill")
	}

	if gmapApiKey == "" {
		logger.Criticalf("[GMAP] api key is empty, please fill")
	}

	emptyGitProject := models.GitProject{}
	if assetsGitProject == emptyGitProject {
		logger.Criticalf("[GIT] assets project is empty, please fill")
	}

	assertFrontendProjects()

	err := assertAssetsProject()
	if err != nil {
		logger.Critical(errors.Trace(err))
	}

	assetsLocalPath := gitProjectPath + git.LocalPath(assetsGitProject.Url)

	httpClient := &fasthttp.Client{}

	// services
	var (
		douClient       = dou.NewClient(httpClient, scrapperCredentials)
		vacancyScrapper = dou.NewDailyVacancyScrapper(dou.NewVacancyScrapper(
			dou.NewVacancyStorage(vacancyStoragePath, 3000),
			douClient,
		))
		companyStorage  = dou.NewCompanyStorage(companyStoragePath, 7*24*3600)
		companyScrapper = dou.NewCompanyScrapper(
			companyStorage,
			douClient,
		)
		reviewScrapper = dou.NewReviewScrapper(
			dou.NewReviewStorage(reviewStoragePath, 8*3600),
			douClient,
		)
		geocodeService = gmap.NewGeocodeService(
			gmap.NewLocationStorage(locationStoragePath),
			gmap.NewClient(httpClient, gmapApiKey),
		)
		companyTypeClient = typeclient.NewClient(httpClient, typeClientPath)
	)

	// global state
	var (
		skipAddresses = make(map[string]bool)
	)

	scrapHandler := func() error {
		startTime := time.Now()
		vacancies, vacanciesStats, err := vacancyScrapper.Scrap()
		vacancyRunDuration := time.Since(startTime)

		logger.Infof("[DOU][VACANCIES] total %d", len(vacancies))
		logger.Infof("[DOU][VACANCIES] stats requests %d, newest count %d, error count %d", vacanciesStats.RequestCount, vacanciesStats.NewestCount, vacanciesStats.ErrorCount)
		logger.Infof("[DOU][VACANCIES] duration %.3f\n\n", asSecond(vacancyRunDuration))

		if err != nil {
			return errors.Trace(err)
		}

		startTime = time.Now()
		companies, companiesStats, err := companyScrapper.ScrapByAliases(dou.ExtractCompanyAliasesByVacancies(vacancies), companyRequestLimit)
		companyRunDuration := time.Since(startTime)

		logger.Infof("[DOU][COMPANIES] total %d", len(companies))
		logger.Infof("[DOU][COMPANIES] stats requests %d, newest count %d, error count %d", companiesStats.RequestCount, companiesStats.NewestCount, companiesStats.ErrorCount)
		logger.Infof("[DOU][COMPANIES] duration %.3f\n\n", asSecond(companyRunDuration))

		if err != nil {
			return errors.Trace(err)
		}

		startTime = time.Now()
		reviews, reviewStats, err := reviewScrapper.DiffScrap()
		reviewRunDuration := time.Since(startTime)

		logger.Infof("[DOU][REVIEWS] companies with reviews %d", len(reviews))
		logger.Infof("[DOU][REVIEWS] stats requests %d, newest count %d, error count %d", reviewStats.RequestCount, reviewStats.NewestCount, reviewStats.ErrorCount)
		logger.Infof("[DOU][REVIEWS] duration %.3f\n\n", asSecond(reviewRunDuration))

		if err != nil {
			return errors.Trace(err)
		}

		startTime = time.Now()
		addressLocationMap, invalidAddresses, gmapStats, err := geocodeService.FetchByAddresses(dou.AddressesByCompanies(companies, skipAddresses))
		gmapRunDuration := time.Since(startTime)

		logger.Infof("[GMAP] total %d", len(addressLocationMap))
		logger.Infof("[GMAP] invalid addresses %d", len(invalidAddresses))
		logger.Infof("[GMAP] stats requests %d, newest count %d, error count %d", gmapStats.RequestCount, gmapStats.NewestCount, gmapStats.ErrorCount)
		logger.Infof("[GMAP] duration %.3f", asSecond(gmapRunDuration))

		if err != nil {
			return errors.Trace(err)
		}

		for _, invalidAddress := range invalidAddresses {
			skipAddresses[invalidAddress] = true
		}

		startTime = time.Now()
		companyAliasTypeMap, err := companyTypeClient.CompanyAliasTypeMap()
		runDuration := time.Since(startTime)

		logger.Infof("[CT] company type records %d", len(companyAliasTypeMap))
		logger.Infof("[CT] duration %.3f", asSecond(runDuration))

		if err != nil {
			logger.Error(errors.Trace(err))

			// NOP
		}

		startTime = time.Now()
		fullCompanies, skipCompanies := dou.Combine(companies, vacancies, reviews, companyAliasTypeMap, addressLocationMap, verifyCompanyMap())
		runDuration = time.Since(startTime)

		fullVacancyCount := 0
		skipVacancyCount := 0
		for _, fullCompany := range fullCompanies {
			fullVacancyCount += len(fullCompany.Vacancies)
		}

		for _, skipCompany := range skipCompanies {
			skipVacancyCount += skipCompany.VacancyCount
		}

		fullCompaniesCount := len(fullCompanies)
		skipCompaniesCount := len(skipCompanies)

		logger.Infof("[COMBINE] companies full %d, skip %d", fullCompaniesCount, skipCompaniesCount)
		logger.Infof("[COMBINE] vacanices full %d, skip %d", fullVacancyCount, skipVacancyCount)
		logger.Infof("[COMBINE] duration %.3f", asSecond(runDuration))

		startTime = time.Now()
		err = git.CloneAndPull(assetsGitProject.Url, assetsLocalPath, assetsGitProject.AuthorName, assetsGitProject.AuthorEmail, assetsGitProject.Branch)
		runDuration = time.Since(startTime)
		logger.Infof("[GIT][ASSETS PULL] duration %.3f", asSecond(runDuration))

		if err != nil {
			logger.Error(errors.Trace(err))

			// NOP
			return nil
		}

		// newest vacancies or update expired companies
		var needUpdate = vacanciesStats.NewestCount > 0 || companiesStats.RequestCount > 0

		var (
			assetRelativePath = assetRelativePath()
			assetFilename     = assetFilename()
		)

		if needUpdate {
			content, err := dumper.Dump(fullCompanies)
			if err != nil {
				logger.Error(errors.Trace(err))

				// NOP
				return nil
			}

			err = ioutil.WriteFile(resultStoragePath, content, 0666)
			if err != nil {
				logger.Error(errors.Trace(err))

				// NOP
				return nil
			}

			currentAssetsLocalPath := assetsLocalPath + assetRelativePath
			err = os.MkdirAll(currentAssetsLocalPath, 0666)
			if err != nil {
				logger.Error(errors.Trace(err))

				// NOP
				return nil
			}

			currentAssetsFilename := currentAssetsLocalPath + "/" + assetFilename
			err = ioutil.WriteFile(currentAssetsFilename, content, 0666)
			if err != nil {
				logger.Error(errors.Trace(err))

				// NOP
				return nil
			}

			err = git.Add(assetsLocalPath, currentAssetsFilename)
			if err != nil {
				logger.Error(errors.Trace(err))

				// NOP
				return nil
			}
		}

		// will commit in any case to show stats
		startTime = time.Now()
		err = git.CommitAndPush(assetsLocalPath, fmt.Sprintf(`
vacancies %d by %d companies
skip vacancies %d by %d companies
vacancies stats: %3d requests, %3d new, %3d error by %.3f
companies stats: %3d requests, %3d new, %3d error by %.3f
review    stats: %3d requests, %3d new, %3d error by %.3f
gmap      stats: %3d requests, %3d new, %3d error by %.3f
top skip companies %s
commit by @qrsx-bot
		`,
			fullVacancyCount, fullCompaniesCount,
			skipVacancyCount, skipCompaniesCount,
			vacanciesStats.RequestCount, vacanciesStats.NewestCount, vacanciesStats.ErrorCount, asSecond(vacancyRunDuration),
			companiesStats.RequestCount, companiesStats.NewestCount, companiesStats.ErrorCount, asSecond(companyRunDuration),
			reviewStats.RequestCount, reviewStats.NewestCount, reviewStats.ErrorCount, asSecond(reviewRunDuration),
			gmapStats.RequestCount, gmapStats.NewestCount, gmapStats.ErrorCount, asSecond(gmapRunDuration),
			skipCompaniesStats(skipCompanies),
		))
		runDuration = time.Since(startTime)

		if err != nil {
			logger.Error(errors.Trace(err))

			// NOP
			return nil
		}

		logger.Infof("[GIT][ASSETS] success commit and push by %.3f", asSecond(runDuration))

		if needUpdate {
			startTime = time.Now()
			refreshFrontendProjects(cdnFile("qrsx/assets@main"+assetRelativePath+"/"+assetFilename), fullCompanies, AllCompanyAliases(companyStorage))
			runDuration = time.Since(startTime)

			logger.Infof("[GIT][FRONTEND] success commit and push by %.3f", asSecond(runDuration))
		}

		return nil
	}

	err = scrapHandler()
	if err != nil {
		logger.Critical(errors.Trace(err))
	}

	var one sync.Mutex
	scrapHandlerSync := func() error {
		one.Lock()
		defer one.Unlock()

		return scrapHandler()
	}

	c := cron.New()

	// ignore parse "spec" error
	err = c.AddFunc("0 1 6,8,9,10,12,14,16,17,18,19,20,21,22,23 * * *", func() {
		err := scrapHandlerSync()

		if err != nil {
			logger.Error(err)

			return
		}
	})

	if err != nil {
		logger.Critical(errors.Trace(err))
	}

	c.Run()
}

func asSecond(duration time.Duration) float64 {
	return float64(duration) / float64(time.Second)
}

func assetRelativePath() string {
	return time.Now().In(kyiv).Format(assetRelativePathFormat)
}

func lastmod() string {
	return time.Now().In(kyiv).Format(lastmodFormat)
}

func assetFilename() string {
	return time.Now().In(kyiv).Format(assetVersionFileFormat)
}

func cdnFile(path string) string {
	return "https://cdn.jsdelivr.net/gh/" + path
}

func skipCompaniesStats(companies []SkipCompany) string {
	var length = len(companies)
	if length == 0 {
		return ""
	}

	var top = 16
	if top > length {
		top = length
	}

	var result = make([]string, top)
	for i := 0; i < top; i++ {
		result[i] = companies[i].Alias
	}

	return strings.Join(result, ",")
}

func verifyCompanyMap() map[string]bool {
	var content, readErr = ioutil.ReadFile(verifyCompanyPath)

	if readErr != nil {
		logger.Error(errors.Trace(readErr))

		return nil
	}

	var companies []string

	var unmarshalErr = json.Unmarshal(content, &companies)
	if unmarshalErr != nil {
		logger.Error(errors.Trace(unmarshalErr))

		return nil
	}

	var result = make(map[string]bool, len(companies))
	for _, company := range companies {
		result[company] = true
	}

	return result
}

func AllCompanyAliases(storage *dou.CompanyStorage) []string {
	var companies, err = storage.Fetch()

	if err != nil {
		logger.Error(errors.Trace(err))

		return nil
	}

	// @todo hotfix, aliases is empty
	var result = make([]string, 0, len(companies))
	for _, company := range companies {
		if company.Alias == "" || company.Logo == "" {
			continue
		}

		result = append(result, company.Alias)
	}
	return result
}
