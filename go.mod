module gitlab.com/qrsx/core

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.6.0 // indirect
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/juju/errors v0.0.0-20200330140219-3fe23663418f
	github.com/klauspost/compress v1.11.4 // indirect
	github.com/mailru/easyjson v0.7.6
	github.com/robfig/cron v1.2.0
	github.com/stretchr/testify v1.6.1
	github.com/valyala/fasthttp v1.18.0
	github.com/valyala/quicktemplate v1.6.3
	gitlab.com/qrsx/companytype-client v0.0.0-20191201234039-6856123164d3
	gitlab.com/qrsx/dou-scrapper v0.0.0-20211003001111-f501649d45da
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b // indirect
)
