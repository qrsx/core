install:
	mkdir -p projects

	sudo mkdir -p /var/proto.storage/dou
	sudo chmod -R 777 /var/proto.storage/dou

	sudo mkdir -p /var/proto.storage/gmap
	sudo chmod -R 777 /var/proto.storage/gmap

	sudo mkdir -p /var/projects.storage
	sudo chmod -R 777 /var/projects.storage
fmt:
	go fmt ./...

easyjson:
	rm -rf vendor
	easyjson ./models/zip.go ./models/glassdoor.go ./models/social.go ./models/legal.go
	go mod vendor
	git add .

run:
	go run *.go

build:
	go build -o /bin/qrsx

generate-template:
	# go get -u github.com/valyala/quicktemplate/qtc
	qtc -dir=views -skipLineComments